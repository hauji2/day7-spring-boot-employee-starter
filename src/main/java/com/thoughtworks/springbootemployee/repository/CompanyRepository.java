package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.CommonUtils;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class CompanyRepository {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    private CommonUtils commonUtils;

    private ArrayList<Company> companies;

    public CompanyRepository() {
        this.companies = new ArrayList<>();
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "boot"));
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }


    public List<Employee> findEmployeesByCompanyId(long companyId) {
        return employeeRepository.findByCompanyId(companyId);
    }

    public List<Company> findByPage(int pageNumber, int pageSize) {
        return companies.stream()
                .skip(commonUtils.countSkipNumber(pageNumber, pageSize))
                .limit(pageSize)
                .toList();
    }

    public Company insertCompany(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1L;
    }

    public Company updateCompany(Company company, Long id) {
        Company targetCompany = findById(id);
        if (Objects.isNull(targetCompany)) {
            return null;
        }

        targetCompany.setName(Objects.nonNull(company.getName()) ? company.getName() : targetCompany.getName());
        return targetCompany;
    }

    public void deleteById(Long id) {
        employeeRepository.deleteByCompanyId(id);
        companies.removeIf((company -> Objects.equals(company.getId(), id)));
    }
}
