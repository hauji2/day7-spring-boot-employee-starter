package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.CommonUtils;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class EmployeeRepository {

    @Autowired
    CommonUtils commonUtils;

    private List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "hauji2", 99, "male", 9999, 1L));
        employees.add(new Employee(2L, "hauji3", 99, "male", 22222, 2L));
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equalsIgnoreCase(gender))
                .toList();
    }

    public Employee insertEmployee(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    private long generateId() {
        return employees.stream()
                .mapToLong(Employee::getId)
                .max()
                .orElse(0L) + 1L;
    }

    public Employee updateEmployee(Employee employee, Long id) {
        Employee targetEmployee = findById(id);

        if (Objects.isNull(targetEmployee)) {
            return null;
        }

        targetEmployee.setAge(Objects.nonNull(employee.getAge()) ? employee.getAge() : targetEmployee.getAge());
        targetEmployee.setSalary(Objects.nonNull(employee.getSalary()) ?
                employee.getSalary() : targetEmployee.getSalary());
        return targetEmployee;
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employees.stream()
                .skip(commonUtils.countSkipNumber(pageNumber, pageSize))
                .limit(pageSize)
                .toList();
    }

    public void deleteById(Long id) {
        employees.removeIf((employee -> Objects.equals(employee.getId(), id)));
    }

    public List<Employee> findByCompanyId(long companyId) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId))
                .toList();
    }

    public void deleteByCompanyId(Long companyId) {
        employees.removeIf((employee -> Objects.equals(employee.getCompanyId(), companyId)));
    }
}
