package com.thoughtworks.springbootemployee.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@AllArgsConstructor
@Data
public class Company implements Serializable {
    private Long id;
    private String name;
}
