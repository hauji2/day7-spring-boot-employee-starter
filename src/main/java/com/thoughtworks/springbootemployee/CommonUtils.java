package com.thoughtworks.springbootemployee;

import org.springframework.stereotype.Component;

@Component
public class CommonUtils {

    public Long countSkipNumber(int pageNumber, int pageSize) {
        return (long) (pageNumber - 1) * pageSize;
    }
}
